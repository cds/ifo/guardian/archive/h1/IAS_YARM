#   IAS_YARM.py
#
# $Id: IAS_YARM.py 9158 2014-11-18 23:27:56Z alexan.staley@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/asc/h1/guardian/IAS_YARM.py $
#

from guardian import GuardState
import IASstates

# initial request on initialization
request = 'IDLE'

global ITMY_BPD1_P
global ITMY_BPD1_Y

# share the global values between IASstates by sending reasonable inital values
IASstates.set_globals('Y', -70, 100.2)

class IDLE(GuardState):
    request = True
    goto = True
    def main(self):
        global ITMY_BPD1_P
        global ITMY_BPD1_Y
        global ITMY_BPD1_P
        global ITMY_BPD1_Y

        # initialize the global values
        ITMY_BPD1_P = 100 
        ITMY_BPD1_Y = 100
        ITMY_BPD1_P = 100
        ITMY_BPD1_Y = 100
        return

# create an instance for a single TMSY alginment
# The numbers were the ones captured as of 23rd of July
ALIGN_TMSY = IASstates.generate_TMSXY()
ALIGN_TMSY.arm = 'Y'
ALIGN_TMSY.xPosTarget = 310 # old = 321
ALIGN_TMSY.yPosTarget = 247.5 # old = 278.3
ALIGN_TMSY.xCal = -4.417
ALIGN_TMSY.yCal = 1.575
ALIGN_TMSY.mu = 0.03
ALIGN_TMSY.GoodError = 0.3

# make an instance
POINTING_ITMY_TO_BPD1 = IASstates.gen_POINT_ITM_TO_BPD()
POINTING_ITMY_TO_BPD1.arm = 'Y'
POINTING_ITMY_TO_BPD1.sus = 'ITMY'
POINTING_ITMY_TO_BPD1.PDnum = 1
POINTING_ITMY_TO_BPD1.itmOffsetPitch = -319.3
POINTING_ITMY_TO_BPD1.itmOffsetYaw = 860.3
POINTING_ITMY_TO_BPD1.SUSmove = 3

# make instances for manual ITMY lock
#[FIYME] this needs to be a function
MANUAL_ITMY = IASstates.generate_MANUAL_ITM()
MANUAL_ITMY.arm = 'Y'
MANUAL_ITMY.PDnum = 1
MANUAL_ITMY.GoodPower = 0.32

# find max
FIND_MAX_BPD1POWER = IASstates.generate_FIND_MAX_BPDPOWER()
FIND_MAX_BPD1POWER.arm = 'Y'
FIND_MAX_BPD1POWER.sus = 'ITMY'
FIND_MAX_BPD1POWER.PDnum = 1
FIND_MAX_BPD1POWER.GoodPower = 0.3

# make a instance
POINTING_ITMY_TO_BPD4 = IASstates.gen_POINT_ITM_TO_BPD()
POINTING_ITMY_TO_BPD4.arm = 'Y'
POINTING_ITMY_TO_BPD4.sus = 'ITMY'
POINTING_ITMY_TO_BPD4.PDnum = 1
POINTING_ITMY_TO_BPD4.itmOffsetPitch = -POINTING_ITMY_TO_BPD1.itmOffsetPitch
POINTING_ITMY_TO_BPD4.itmOffsetYaw = -POINTING_ITMY_TO_BPD1.itmOffsetYaw
POINTING_ITMY_TO_BPD4.SUSmove = 6 # a bit longer waiting time


# check second baffle PD
CHECK_SECOND_BPDY = IASstates.generate_CHECK_SECOND_BPD()
CHECK_SECOND_BPDY.arm = 'Y'
CHECK_SECOND_BPDY.PDnum = 4
CHECK_SECOND_BPDY.GoodPower = 0.2

# make instances for ETMY alignment state
ALIGN_ETMY = IASstates.generate_ALIGNING_ETM()
ALIGN_ETMY.arm = 'Y'
ALIGN_ETMY.itmGreenX = 310 # old = 314 
ALIGN_ETMY.itmGreenY = 247 # old = 240
ALIGN_ETMY.error = 10 # initial error
ALIGN_ETMY.GoodError = 3
ALIGN_ETMY.mu = 0.25
ALIGN_ETMY.xCal = -14
ALIGN_ETMY.yCal = -8.2

# make instances for ALIGN_ETMY_GREEN_LOCKED
ALIGN_ETMY_GREEN_LOCKED = IASstates.generate_ALIGNING_ETM_GREEN_LOCKED()
ALIGN_ETMY_GREEN_LOCKED.arm = 'Y'
ALIGN_ETMY_GREEN_LOCKED.itmGreenX = 310 # old = 314
ALIGN_ETMY_GREEN_LOCKED.itmGreenY = 247 # old = 240
ALIGN_ETMY_GREEN_LOCKED.error = 5 # initial error
ALIGN_ETMY_GREEN_LOCKED.GoodError = 1
ALIGN_ETMY_GREEN_LOCKED.mu = 0.1
ALIGN_ETMY_GREEN_LOCKED.xCal = -14
ALIGN_ETMY_GREEN_LOCKED.yCal = -8.2

# make instance for maximizimg sweep
MAXIMIZING_TRY = IASstates.generate_MAXIMIZING_TR()
MAXIMIZING_TRY.arm = 'Y'

# make instances for the aligned states
ITMY_ALIGNED = IASstates.generate_ALIGNED()
ITMY_ALIGNED.SUS = 'ITMY'

ETMY_ALIGNED = IASstates.generate_ALIGNED()
ETMY_ALIGNED.SUS = 'ETMY'

TMSY_ALIGNED = IASstates.generate_ALIGNED()
TMSY_ALIGNED.SUS = 'TMSY'


##################################################
edges = [
    ('IDLE', 'ALIGN_ETMY'),
    ('ALIGN_ETMY', 'ALIGN_ETMY_GREEN_LOCKED'),
    ('ALIGN_ETMY_GREEN_LOCKED', 'MAXIMIZING_TRY'),
    ('MAXIMIZING_TRY', 'ETMY_ALIGNED'),
    ('IDLE', 'POINTING_ITMY_TO_BPD1'),
    ('POINTING_ITMY_TO_BPD1', 'FIND_MAX_BPD1POWER'),
    ('FIND_MAX_BPD1POWER', 'POINTING_ITMY_TO_BPD4'),
    ('POINTING_ITMY_TO_BPD4', 'CHECK_SECOND_BPDY'),
    ('FIND_MAX_BPD1POWER', 'MANUAL_ITMY'),
    ('CHECK_SECOND_BPDY', 'ITMY_ALIGNED'),
    ('MANUAL_ITMY', 'POINTING_ITMY_TO_BPD1'),
    ('IDLE', 'ALIGN_TMSY'),
    ('ALIGN_TMSY', 'TMSY_ALIGNED'),
 ]
